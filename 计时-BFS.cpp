#include <iostream>
#include <vector>
#include <string>
#include <cstdlib>
#include <cstdio>
#include <cstdio>
#include <map>
#include <ctime>

using namespace std;

const int INF = 1 << 28;
const int MAXN = 500 + 5;
vector< vector<int> > ori(MAXN);
vector<bool> DFS_visited(MAXN);
int routes[MAXN][MAXN];
int mini = INF;
const int runNum = 100000;

std::vector<std::string> split(std::string str,std::string pattern)
{
    std::string::size_type pos;
    std::vector<std::string> result;
    str += pattern;
    int size=str.size();

    for(int i=0; i<size; i++)
    {
        pos=str.find(pattern,i);
        if(pos<(unsigned int)size)
        {
            std::string s=str.substr(i,pos-i);
            result.push_back(s);
            i=pos+pattern.size()-1;
        }
    }
    return result;
}
void DFS(int cur, int stop, int change, int last, int cur_route)
{
    if(DFS_visited[cur] || change > mini)
        return;
    int n_route = routes[last][cur];
    int n_change = change;
    if(n_route != cur_route)
    {
        n_change ++;
    }
    if(cur == stop)
    {
        if(n_change < mini)
        {
            mini = n_change;
        }
        return;
    }
    DFS_visited[cur] = true;
    for (int i = 0; i < ori[cur].size(); i ++)
    {
        DFS(ori[cur][i], stop, n_change, cur, n_route);
    }
    DFS_visited[cur] = false;
}
int main()
{
    freopen("input.txt", "r", stdin);
    int M, N;
    int st, ne;
    cin >> M >> N;
    vector<string> tmp;
    getchar();
    string bus;
    for (int i = 1; i <= M; i ++)
    {
        getline(cin, bus);
        tmp = split(bus, " ");
        if(tmp.size() != 0){
            sscanf(tmp[0].c_str(), "%d", &st);
        }
        for (unsigned int j = 1; j < tmp.size(); j ++)
        {
            sscanf(tmp[j].c_str(), "%d", &ne);
            ori[st].push_back(ne);
            routes[st][ne] = routes[ne][st] = i;
            st = ne;
        }
    }
    clock_t begin_t, end_t;
    begin_t = clock();
    for (int i = 0; i < runNum; i ++)
    {
        mini = INF;
        DFS_visited.resize(MAXN, 0);
        DFS(1, N, -1, 1, 0);
    }
    end_t = clock();
    cout << "Total cost : " << (end_t - begin_t) << "ms" << endl;
//    if(mini == -1)
//        mini = 0;
//    if(mini == INF)
//        cout << "NO" << endl;
//    else
//        cout << mini << endl;
    return 0;
}









