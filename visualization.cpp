#include <iostream>
#include <vector>
#include <string>
#include <cstdlib>
#include <cstdio>
#include <map>
#include <sstream>
using namespace std;

int M, N;
const int INF = 1 << 28;
const int MAXN = 500 + 5;
const vector<int> nullVe;
vector< vector<int> > ori(MAXN);
vector< vector<int> > input;
vector<bool> DFS_visited(MAXN);
vector< vector<int> > res;
vector<int> ans, tmp;
int routes[MAXN][MAXN];
int mini = INF;
int png = 0;
vector<string> colors(50);

void init ()
{
    colors[0] = "antiquewhite";
    colors[1] = "goldenrod2";
    colors[2] = "aquamarine";
    colors[3] = "beige";
    colors[4] = "bisque";
    colors[5] = "blanchedalmond";
    colors[6] = "blue";
    colors[7] = "brown";
    colors[8] = "cadetblue";
    colors[9] = "chartreuse";
    colors[10] = "chocolate";
    colors[11] = "coral";
    colors[12] = "cornflowerblue";
    colors[13] = "crimson";
    colors[14] = "cyan";
    colors[15] = "pink";
    colors[16] = "darkgreen";
    colors[17] = "yellow";
    colors[18] = "darkorange";
    colors[19] = "darkorchid";
    colors[20] = "darksalmon";
    colors[21] = "darkseagreen";
    colors[22] = "darkturquoise";
    colors[23] = "darkviolet";
    colors[24] = "deeppink";
    colors[25] = "deepskyblue";
    colors[26] = "dodgerblue";
    colors[27] = "firebrick";
    colors[28] = "fuchsia";
    colors[29] = "gold";
    colors[30] = "greenyellow";
    colors[31] = "hotpink";
    colors[32] = "khaki";
    colors[33] = "lemonchiffon";
    colors[34] = "lightblue";
    colors[35] = "lightcoral";
    colors[36] = "lightcyan";
    colors[37] = "lightsalmon";
    colors[38] = "lightseagreen";vector< vector<int> > input;
    colors[39] = "mediumorchid";
    colors[40] = "mediumspringgreen";
}

std::vector<std::string> split(std::string str,std::string pattern)
{
    std::string::size_type pos;
    std::vector<std::string> result;
    str += pattern;
    int size=str.size();

    for(int i=0; i<size; i++)
    {
        pos=str.find(pattern,i);
        if(pos<(unsigned int)size)
        {
            std::string s=str.substr(i,pos-i);
            result.push_back(s);
            i=pos+pattern.size()-1;
        }
    }
    return result;
}
void DFS(int cur, int stop, int change, int last, int cur_route)
{
    //if(DFS_visited[cur] || change > mini)
    if(DFS_visited[cur])
        return;
    int n_route = routes[last][cur];
    int n_change = change;
    if(n_route != cur_route)
    {
        n_change ++;
    }
    if(cur == stop)
    {
        vector<int> ttmp = tmp;
        ttmp.push_back(stop);
        res.push_back(ttmp);
        if(n_change < mini || (n_change == mini && ttmp.size() < ans.size()))
        {
            mini = n_change;
            ans = ttmp;
        }
        return;
    }
    tmp.push_back(cur);
    DFS_visited[cur] = true;
    for (int i = 0; i < ori[cur].size(); i ++)
    {freopen("out.txt", "w", stdout);
        DFS(ori[cur][i], stop, n_change, cur, n_route);
    }
    DFS_visited[cur] = false;
    tmp.pop_back();
}

void generate_png(vector< vector<int> > ori, string name, vector<int> solution = nullVe)
{
    freopen((name + ".gv").c_str(), "w", stdout);
    cout << "digraph " << name << " " << "{ " << endl;
    for (int i = 1; i <= N; i ++)
    {
        cout << i << "[color = " << colors[i] << "]" << endl;
    }
    for (int i = 0; i < ori.size(); i ++)
    {
        vector<int> out = ori[i];
        if(out.size() != 0)
            cout << out[0];
        for (int j = 1; j < out.size(); j ++)
        {
            cout << " -> " << out[j];
        }
        cout << endl;
    }
    if(solution.size() != 0)
    {
        cout << solution[0];
        for (int j = 1; j < solution.size(); j ++)
        {
            cout << " -> " << solution[j];
        }
        cout << "[color=" << colors[(10+(++png) % 40)] << "]";
        cout << endl;
    }
    cout << "} " << endl;
    string cmd = "dot " + name + ".gv -Tpng -o " + name+ ".png";
    system(cmd.c_str());
}
int main()
{
    init();
    freopen("input.txt", "r", stdin);
    int st, ne;
    cin >> M >> N;
    vector<string> stmp;
    getchar();
    string bus;
    for (int i = 1; i <= M; i ++)
    {
        getline(cin, bus);
        vector<int> input_tmp;
        stmp = split(bus, " ");
        if(stmp.size() != 0){
            sscanf(stmp[0].c_str(), "%d", &st);
            input_tmp.push_back(st);
        }
        for (unsigned int j = 1; j < stmp.size(); j ++)
        {
            sscanf(stmp[j].c_str(), "%d", &ne);
            input_tmp.push_back(ne);
            ori[st].push_back(ne);
            routes[st][ne] = routes[ne][st] = i;
            st = ne;
        }
        input.push_back(input_tmp);
    }
    tmp.clear();
    ans.clear();
    res.clear();
    DFS(1, N, -1, 1, 0);
    freopen("out.txt", "w", stdout);
    cout << "Answer : ";
    if(mini == -1)
        mini = 0;
    if(mini == INF)
        cout << "NO" << endl;
    else
        cout << mini << endl;
    cout << "Avaliable solution(s)" << endl;
    for (int i = 0; i < res.size(); i ++)
    {
        vector<int> xyz = res[i];
        for (int j = 0; j < xyz.size(); j ++)
        {
            cout << xyz[j] << " ";
        }
        cout << endl;
    }
    cout << "Best solution" << endl;
    for (int i = 0; i < ans.size(); i ++)
    {
        cout << ans[i] << " ";
    }
    cout << endl;
    generate_png(input, "0");
    for (int i = 0; i < res.size(); i ++)
    {
        stringstream ss;
        string out_name = "";
        ss << i + 1;
        out_name = ss.str();
        generate_png(input, out_name, res[i]);
    }
    generate_png(input, "FAST", ans);
    return 0;
}









