// BFS最小换乘.cpp : 定义控制台应用程序的入口点。
//


#include <iostream>
#include <vector>
#include <string>
#include <cstdlib>
#include <cstdio>
#include <cstdio>
#include <map>
#include <utility>
#include <queue>

using namespace std;

const int INF = 1 << 28;
const int MAXN = 500 + 5;
vector< vector<int> > ori(MAXN);
vector<bool> BFS_visited(MAXN);
int routes[MAXN][MAXN];
int mini = INF;
int M, N;
std::vector<std::string> split(std::string str,std::string pattern)
{
	std::string::size_type pos;
	std::vector<std::string> result;
	str += pattern;
	int size=str.size();

	for(int i=0; i<size; i++)
	{
		pos=str.find(pattern,i);
		if(pos<(unsigned int)size)
		{
			std::string s=str.substr(i,pos-i);
			result.push_back(s);
			i=pos+pattern.size()-1;
		}
	}
	return result;
}
int BFS(int start)
{
	int min_change = INF;
	queue< pair<int, int> > qu;
	vector<int> ans(MAXN, 0);
	//BFS_visited[start] = true;
	for (int j = 0; j < ori[start].size(); j ++)
	{
		qu.push(pair<int, int>(start, ori[start][j]));
		//BFS_visited[ori[start][j]] = true;
		if(ori[start][j] == N)
			return 0;
	}
	while(!qu.empty())
	{
		pair<int, int> tmp = qu.front();
		qu.pop();
		for (int i = 0; i < ori[tmp.second].size(); i ++)
		{
			//if(!BFS_visited[ori[tmp.second][i]] || ori[tmp.second][i] == N)
			//{
				if (ori[tmp.second][i] != N)		//如果已经到了站点N，那么不必Push
				{
					qu.push(pair<int, int>(tmp.second, ori[tmp.second][i]));
				}
				//BFS_visited[ori[tmp.second][i]] = true;
				int n_change = ans[tmp.second];
				if(routes[tmp.first][tmp.second] != routes[tmp.second][ori[tmp.second][i]])
					n_change ++;
				ans[ori[tmp.second][i]] = n_change;
				if(ori[tmp.second][i] == N && ans[ori[tmp.second][i]] < min_change )
					min_change=ans[ori[tmp.second][i]];
			//}

			/*if(!BFS_visited[ori[tmp.second][i]] || ori[tmp.second][i] == N)
			{
				qu.push(pair<int, int>(tmp.second, ori[tmp.second][i]));
				BFS_visited[ori[tmp.second][i]] = true;
				int n_change = ans[tmp.second];
				if(routes[tmp.first][tmp.second] != routes[tmp.second][ori[tmp.second][i]])
					n_change ++;
				ans[ori[tmp.second][i]] = n_change;
				if(ori[tmp.second][i] == N && ans[ori[tmp.second][i]] < min_change )
					min_change=ans[ori[tmp.second][i]];
			}*/

			//qu.push(pair<int, int>(tmp.second, ori[tmp.second][i]));
			//int n_change = ans[tmp.second];
			//if(routes[tmp.first][tmp.second] != routes[tmp.second][ori[tmp.second][i]])
			//	n_change ++;
			//ans[ori[tmp.second][i]] = n_change;
			//if(ori[tmp.second][i] == N && ans[ori[tmp.second][i]] < min_change )
			//	min_change=ans[ori[tmp.second][i]];

		}
	}
	return min_change;
}
int main()
{
	freopen("input.txt", "r", stdin);
	int st, ne;
	cin >> M >> N;
	vector<string> tmp;
	getchar();
	string bus;
	for (int i = 1; i <= M; i ++)
	{
		getline(cin, bus);
		tmp = split(bus, " ");
		if(tmp.size() != 0){
			sscanf(tmp[0].c_str(), "%d", &st);
		}
		for (unsigned int j = 1; j < tmp.size(); j ++)
		{
			sscanf(tmp[j].c_str(), "%d", &ne);
			ori[st].push_back(ne);
			//routes[st][ne] = routes[ne][st] = i;
			routes[st][ne] = i;
			st = ne;
		}
	}
	int pr = BFS(1);
	if(pr == INF)
		cout << "NO" << endl;
	else
		cout << pr << endl;
	return 0;
}








