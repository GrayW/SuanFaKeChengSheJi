#include<cstdio>
#include<cstring>
#include<iostream>
#include<cstdlib>
#include<ctime>
#define inf 0xfffffff
#define N 505
using namespace std;
const int runNum = 100000;
int dis[N],map[N][N],visited[N],n,m,a[5005];
void dijkstra()
{
    int m,k;
    for(int i=1;i<=n;i++)
    {
        m=inf;
        for(int j=1;j<=n;j++)
        {
            if(!visited[j]&&dis[j]<m)
            {
                m=dis[j];
                k=j;
            }
        }
        if(m==inf) break;

        visited[k]=1;
        for(int j=1;j<=n;j++)
        {
            if(!visited[j])
            {
                if(dis[j]>dis[k]+map[k][j])
                    dis[j]=dis[k]+map[k][j];
            }
        }
    }
}
int main()
{
    freopen("input.txt","r",stdin);
    int s;
    char as;
    while(scanf("%d%d",&m,&n)!=EOF)
    {
        for(int i=1;i<=m;i++)
        {
            s=0;as=1;
            while(as!=10)
            {
                s++;
                scanf("%d",&a[s]);
                as=getchar();
                if(as==-1)
                    break;
            }

            for(int j=1;j<s;j++)
                for(int l=j+1;l<=s;l++)
                map[a[j]][a[l]]=1;
        }
        clock_t begin_t, end_t;
        begin_t = clock();
        for (int i = 0; i < runNum; i ++)
        {
            memset(visited,0,sizeof(visited));
            memset(a,0,sizeof(a));
            for(int i=1;i<=n;i++)
            {
                dis[i]=inf;
                for(int j=1;j<=n;j++)
                map[i][j]=inf;
            }
            dis[1]=0;
            dijkstra();
//            if(dis[n]>=inf) printf("NO\n");
//            else printf("%d\n",dis[n]-1);

        }
        end_t = clock();
        cout << "Total Cost : " << (end_t - begin_t) << "ms" << endl;
    }
    return 0;
}

