import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.awt.event.ActionEvent;
import java.awt.Font;
import javax.swing.JTextPane;
import javax.swing.JEditorPane;
import javax.swing.JTextArea;

public class HelloWorld {

	private JFrame frame;
	private JLabel label_1;
	private JLabel label_2;
	private JButton button;
	private JTextArea textAreaOutput;
	private JTextArea textAreaInput;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					HelloWorld window = new HelloWorld();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public HelloWorld() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setTitle("最佳换乘问题可视化展示集成系统");
		frame.setBounds(100, 100, 668, 520);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JButton btnSubmit = new JButton("执行");
		btnSubmit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				// 1.先将输入框中的内容存入22.in中
				String test = textAreaInput.getText();
				System.out.println(test);
				try {
					FileWriter fw = new FileWriter("input.txt");
					fw.write(test);
					fw.close();
					System.out.println("写入成功");
				} catch (IOException e) {
					e.printStackTrace();
				}
				// 删除文件夹下面所有.png和gv后缀结尾的文件
				File f = new File(".");
				File[] fi = f.listFiles();
				for (File file : fi) {
					String suffix = file.getName().substring(file.getName().lastIndexOf(".") + 1);
					if (suffix.equals("gv") || suffix.equals("png")) {
						System.out.println("成功删除" + file.getName());
						file.delete();
					}
				}
				File outFile = new File("out.txt");
				outFile.delete();
				System.out.println("删除out.txt成功");

				// 通过命令行调用algo.exe生成图片并生成output.txt
				try {
					Runtime.getRuntime().exec("cmd /c algo.exe");
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				// 将输出结果读到GUI中
				//先等待1.5s,让C语言执行完毕并创建out.txt
				try {
					Thread.sleep(1500);
				} catch (InterruptedException e1) {
					e1.printStackTrace();
				}
				String output = null;
				FileReader fr;
				try {
					fr = new FileReader("out.txt");
					int ch = 0;
					while ((ch = fr.read()) != -1)// fr.read()读取一个字节，判断此字节的值为-1表示读到文件末尾了。
						output += (char) ch;
						//// System.out.println((char)ch);
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
				output = output.substring(4);		//前面会出现null
				textAreaOutput.setText(output);
				
			}
		});
		btnSubmit.setBounds(0, 421, 113, 27);
		frame.getContentPane().add(btnSubmit);

		label_1 = new JLabel("输入：");
		label_1.setFont(new Font("宋体", Font.PLAIN, 20));
		label_1.setBounds(0, 74, 72, 18);
		frame.getContentPane().add(label_1);

		label_2 = new JLabel("输出：");
		label_2.setFont(new Font("宋体", Font.PLAIN, 20));
		label_2.setBounds(335, 74, 72, 18);
		frame.getContentPane().add(label_2);

		button = new JButton("点击进入可视化模块");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String url = "index.html"; // 将这个字符串变量替换成我们的展示页面
				try {
					Runtime.getRuntime().exec("cmd /c start " + url);
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		button.setBounds(430, 421, 206, 27);
		frame.getContentPane().add(button);

		textAreaInput = new JTextArea();
		textAreaInput.setBounds(0, 95, 282, 204);
		frame.getContentPane().add(textAreaInput);

		textAreaOutput = new JTextArea();
		textAreaOutput.setBounds(335, 95, 315, 204);
		frame.getContentPane().add(textAreaOutput);
	}
}
